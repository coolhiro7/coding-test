// URL : https://school.programmers.co.kr/learn/courses/30/lessons/138476

#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <unordered_map>

using namespace std;

int solution(int k, vector<int> tangerine) {
    int answer = 0;
    unordered_map<int, int> counter;
    vector<int> refined_list;

    for (size_t i = 0; i < tangerine.size(); i++) {
        if (counter.find(tangerine[i]) == counter.end()) {
            counter[tangerine[i]] = 1;
            continue;
        }

        counter[tangerine[i]]++;
    }

    for (auto iter = counter.begin(); iter != counter.end(); iter++) {
        refined_list.emplace_back(iter->second);
    }

    sort(refined_list.begin(), refined_list.end(), [=](int a, int b) {
        return a > b;
    });

    int sum = 0;
    for (size_t i = 0; i < refined_list.size(); i++) {
        sum += refined_list[i];
        answer++;
        if (sum >= k) {
            break;
        }
    }
    
    return answer;
}