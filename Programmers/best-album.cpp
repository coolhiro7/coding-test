// URL : https://school.programmers.co.kr/learn/courses/30/lessons/42579
// 소요 시간 : 30분

#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector<int> solution(vector<string> genres, vector<int> plays) {
    vector<int> answer;
    unordered_map<string, int> play_count;
    unordered_map<string, int> count;

    for (size_t i = 0; i < genres.size(); i++) {
        // 전체 play count 를 세기 위한 map
        string genre = genres[i];
        int play = plays[i];

        play_count[genre] += play;

        if (count.find(genre) == count.end()) {
            count[genre] = 1;
            continue;
        }
        else {
            // 장르의 개수를 세기 위한 map
            count[genre]++;
        }
    }

    // 가장 play 를 많이 한 순으로 소팅하기 위한 작업
    vector<pair<string, int>> vec(play_count.begin(), play_count.end());
    sort(vec.begin(), vec.end(),
        [=](pair<string, int>& a, pair<string, int>& b) {
            return a.second > b.second;
        });

    // pair<int, int> : plays, index
    // 최종으로는 index 를 가져갈 것이므로 index 값을 저장하기 위한 pair 를 이용
    // 사실 index만 가져가므로 plays 값은 필요 없으나 혹시나 해서 추가
    // 장르의 개수는 최대 100개 이므로 100개로 array 고정
    vector<pair<int, int>> list[100];
    for (size_t i = 0; i < genres.size(); i++) {
        size_t index = 0;
        for (size_t j = 0; j < vec.size(); j++) {
            if (genres[i].compare(vec[j].first) == 0) {
                index = j;
                break;
            }
        }

        list[index].push_back({ plays[i], i });
    }

    // 각각의 array 에 대한 sort
    // 이렇게 하면 장르별 vector를 하나씩 가지며 각 장르별로 많이 재생한 순으로 소팅 된다.
    for (size_t i = 0; i < vec.size(); i++) {
        sort(list[i].begin(), list[i].end(),
            [=](pair<int, int>& a, pair<int, int>& b) {
                return a.first > b.first;
            });
    }

    // 베스트 앨범 추출. 최대 2개까지 이므로 그에 맞게끔 설정
    for (size_t i = 0; i < play_count.size(); i++) {
        if (list[i].size() == 1) {
            answer.push_back(list[i][0].second);
        }
        else {
            answer.push_back(list[i][0].second);
            answer.push_back(list[i][1].second);
        }
    }

    return answer;
}