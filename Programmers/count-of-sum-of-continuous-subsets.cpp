// URL : https://school.programmers.co.kr/learn/courses/30/lessons/131701
// 소요 시간 : 10분

#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

int solution(vector<int> elements) {
    int answer = 0;

    unordered_map<int, int> result;

    for (size_t i = 1; i <= elements.size(); i++) {

        for (size_t j = 0; j < elements.size(); j++) {
            size_t index = j;
            size_t sum = 0, count = 0;

            while (count < i) {
                sum += elements[index];
                count++;
                // 순환 수열이므로 인덱스 정리 필요
                if (index == elements.size() - 1) {
                    index = 0;
                    // 아래 구문을 타지 않게끔 continue 사용
                    continue;
                }
                else {
                    index++;
                }
            } // end while (count < i)

            // 중복 개수는 의미가 없으므로 카운트 용도로 사용
            result[sum] = 1;
        } // end for (size_t j = 0; j < elements.size(); j++)
    } // end for (size_t i = 1; i <= elements.size(); i++)

    answer = result.size();

    return answer;
}