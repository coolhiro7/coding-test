// URL : https://school.programmers.co.kr/learn/courses/30/lessons/132265
// time : 15 mins

#include <string>
#include <vector>
#include <map>

using namespace std;

int solution(vector<int> topping) {
    int answer = -1;
    map<int, int> left, right;

    // O(n)을 만족 시키기 위해 right 에 먼저 map 으로 채워넣고
    // left를 하나씩 채울 때 right를 하나씩 제거하면서
    // 토핑 수를 카운트 하는 형태로 진행
    for (auto i = 0; i < topping.size(); i++) {
        if (right.find(topping[i]) == right.end()) {
            right[topping[i]] = 1;
            continue;
        } else {
            right[topping[i]]++;
        }
    }

    for (auto i = 0; i < topping.size() - 1; i++) {
        if (left.find(topping[i]) != left.end()) {
            left[topping[i]]++;
        } else {
            left[topping[i]] = 1;
        }

        right[topping[i]]--;
        if (right[topping[i]] == 0) {
            right.erase(topping[i]);
        }

        if (left.size() == right.size()) {
            answer++;
        }
    }

    return answer;
}